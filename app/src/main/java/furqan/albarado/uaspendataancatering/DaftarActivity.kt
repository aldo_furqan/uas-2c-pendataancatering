package furqan.albarado.uaspendataancatering

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import furqan.albarado.ujiuas.MediaHelperDaftar
import kotlinx.android.synthetic.main.activity_daftar.*
import kotlinx.android.synthetic.main.activity_daftar.btnFind
import org.json.JSONArray
import kotlin.collections.HashMap

class DaftarActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelperDaftar : MediaHelperDaftar
    lateinit var dfAdapter : AdapterDaftar
    var dfDaftar = mutableListOf<HashMap<String,String>>()
    val mainUrl = "http://192.168.43.170/www/cat/"
    val url = mainUrl+"show_daftar.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daftar)

        dfAdapter = AdapterDaftar(dfDaftar,this)
        mediaHelperDaftar = MediaHelperDaftar(this)
        lsDaftar.layoutManager = LinearLayoutManager(this)
        lsDaftar.adapter = dfAdapter

        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnFind -> {
                showDataDaftar(edActDaftarNama.text.toString().trim())
            }
        }
    }

    fun showDataDaftar(dfNama : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                Log.i("info", "["+response+"]")
                dfDaftar.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var df = HashMap<String,String>()
                    df.put("nama_cat",jsonObject.getString("nama_cat"))
                    df.put("nama_menu",jsonObject.getString("nama_menu"))
                    df.put("email",jsonObject.getString("email"))
                    df.put("nama_pemilik",jsonObject.getString("nama_pemilik"))
                    df.put("url",jsonObject.getString("url"))
                    df.put("urlmenu",jsonObject.getString("urlmenu"))
                    dfDaftar.add(df)
                }
                dfAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val df = HashMap<String, String>()
                df.put("nama_cat",dfNama)
                return df
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onStart() {
        super.onStart()
        showDataDaftar("")
    }

}