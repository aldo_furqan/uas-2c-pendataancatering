package furqan.albarado.uaspendataancatering

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*

class AdapterMenu(val dataMenu : List<HashMap<String,String>>, val menuActivity: MenuActivity) :
    RecyclerView.Adapter<AdapterMenu.HolderDataMenu>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterMenu.HolderDataMenu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_menu,p0,false)
        return  HolderDataMenu(v)
    }

    override fun getItemCount(): Int {
        return dataMenu.size
    }

    override fun onBindViewHolder(p0: AdapterMenu.HolderDataMenu, p1: Int) {
        val data = dataMenu.get(p1)
        p0.txrowMenuNama.setText(data.get("nama_menu"))
        if(p1.rem(2) == 0) p0.cMenu.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cMenu.setBackgroundColor(Color.rgb(255,255,245))

        p0.cMenu.setOnClickListener({
            menuActivity.idMenu = data.get("id_menu").toString()
            menuActivity.edActMenuNama.setText(data.get("nama_menu"))
            Picasso.get().load(data.get("url")).into(menuActivity.imvMenu)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photoa)
    }

    class HolderDataMenu(v: View) : RecyclerView.ViewHolder(v){
        val txrowMenuNama = v.findViewById<TextView>(R.id.txrowMenuNama)
        val photoa = v.findViewById<ImageView>(R.id.imvRowMenu)
        val cMenu = v.findViewById<ConstraintLayout>(R.id.cMenu)
    }
}