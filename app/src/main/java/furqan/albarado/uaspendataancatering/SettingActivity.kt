package furqan.albarado.uaspendataancatering

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*


class SettingActivity: AppCompatActivity(), View.OnClickListener {

    var background: String = ""
    var selectBack: String = ""
    var selectBackDaf: String = ""
    var selectBackMenu: String = ""
    var selectBackCat: String = ""
    var selectBackPemilik: String = ""
    var currentSpin = ""
    var currentSpinDaf = ""
    var currentSpinMenu = ""
    var currentSpinCat = ""
    var currentSpinPemilik = ""
    var checkRadio = ""
    var checkHeader = ""

    lateinit var adapterSpin: ArrayAdapter<String>
    val arrayBg = arrayOf("Blue", "Yellow", "Green", "Black", "Red")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"

    //    spinner
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgHeader = "header"
    val bgDaftar = "daftar"
    val bgMenu = "menu"
    val bgCat = "cat"
    val bgPemilik = "pemilik"
    val DEF_BACK_FITUR = "Green"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEdit = preferences.edit()
        prefEdit.putString(bgMain, selectBack)
        prefEdit.putString(bgHeader, checkRadio)
        prefEdit.putString(bgDaftar, selectBackDaf)
        prefEdit.putString(bgMenu, selectBackMenu)
        prefEdit.putString(bgCat, selectBackCat)
        prefEdit.putString(bgPemilik, selectBackPemilik)
        prefEdit.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentSpin = preferences.getString(bgMain, DEF_BACK).toString()
        currentSpinDaf = preferences.getString(bgDaftar, DEF_BACK_FITUR).toString()
        currentSpinMenu = preferences.getString(bgMenu, DEF_BACK_FITUR).toString()
        currentSpinCat = preferences.getString(bgCat, DEF_BACK_FITUR).toString()
        currentSpinPemilik = preferences.getString(bgPemilik, DEF_BACK_FITUR).toString()
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayBg)
        checkHeader = preferences.getString(bgHeader, DEF_BACK).toString()
        spBackground.adapter = adapterSpin
        spBackground.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if (background.equals("Blue")) {
                    selectBack = "Blue"
                }
                if (background.equals("Yellow")) {
                    selectBack = "Yellow"
                }
                if (background.equals("Green")) {
                    selectBack = "Green"
                }
                if (background.equals("Black")) {
                    selectBack = "Black"
                }
                if (background.equals("Red")) {
                    selectBack = "Red"
                }
            }
        }

        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rdBlue -> {
                    checkRadio = "Blue"
                }
                R.id.rdYellow -> {
                    checkRadio = "Yellow"
                }
                R.id.rdGreen -> {
                    checkRadio = "Green"
                }
                R.id.rdBlack -> {
                    checkRadio = "Black"
                }
            }
        }

        spWarnaDaftar.adapter = adapterSpin
        spWarnaDaftar.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if (background.equals("Blue")) {
                    selectBackDaf = "Blue"
                }
                if (background.equals("Yellow")) {
                    selectBackDaf = "Yellow"
                }
                if (background.equals("Green")) {
                    selectBackDaf = "Green"
                }
                if (background.equals("Black")) {
                    selectBackDaf = "Black"
                }
                if (background.equals("Red")) {
                    selectBackDaf = "Red"
                }
            }
        }

        spWarnaMenu.adapter = adapterSpin
        spWarnaMenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if (background.equals("Blue")) {
                    selectBackMenu = "Blue"
                }
                if (background.equals("Yellow")) {
                    selectBackMenu = "Yellow"
                }
                if (background.equals("Green")) {
                    selectBackMenu = "Green"
                }
                if (background.equals("Black")) {
                    selectBackMenu = "Black"
                }
                if (background.equals("Red")) {
                    selectBackMenu = "Red"
                }
            }
        }

        spWarnaCatering.adapter = adapterSpin
        spWarnaCatering.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if (background.equals("Blue")) {
                    selectBackCat = "Blue"
                }
                if (background.equals("Yellow")) {
                    selectBackCat = "Yellow"
                }
                if (background.equals("Green")) {
                    selectBackCat = "Green"
                }
                if (background.equals("Black")) {
                    selectBackCat = "Black"
                }
                if (background.equals("Red")) {
                    selectBackCat = "Red"
                }
            }
        }

        spWarnaPemilik.adapter = adapterSpin
        spWarnaPemilik.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if (background.equals("Blue")) {
                    selectBackPemilik = "Blue"
                }
                if (background.equals("Yellow")) {
                    selectBackPemilik = "Yellow"
                }
                if (background.equals("Green")) {
                    selectBackPemilik = "Green"
                }
                if (background.equals("Black")) {
                    selectBackPemilik = "Black"
                }
                if (background.equals("Red")) {
                    selectBackPemilik = "Red"
                }
            }
        }

        btnSimpan.setOnClickListener(this)
    }


    override fun onStart() {
        super.onStart()
        spBackground.setSelection(getIndex(spBackground, currentSpin))
        spWarnaDaftar.setSelection(getIndex(spWarnaDaftar, currentSpinDaf))
        spWarnaMenu.setSelection(getIndex(spWarnaMenu, currentSpinMenu))
        spWarnaCatering.setSelection(getIndex(spWarnaCatering, currentSpinCat))
        spWarnaPemilik.setSelection(getIndex(spWarnaPemilik, currentSpinPemilik))
        getChecked()
    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        var a = spinner.count
        var b: String = ""
        for (i in 0 until a) {
            b = arrayBg.get(i)
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun getChecked() {
        if (checkHeader.equals("Blue")) {
            rdBlue.isChecked = true
        }
        if (checkHeader.equals("Green")) {
            rdGreen.isChecked = true
        }
        if (checkHeader.equals("Yellow")) {
            rdYellow.isChecked = true
        }
        if (checkHeader.equals("Black")) {
            rdBlack.isChecked = true
        }
    }
}