package furqan.albarado.uaspendataancatering

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_pemilik.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class PemilikActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var pemilikAdapter : AdapterPemilik
    var daftarPemilik = mutableListOf<HashMap<String,String>>()
    var idPemilik = ""
    val mainUrl = "http://192.168.43.170/www/cat/"
    val url = mainUrl+"show_pemilik.php"
    val url2 = mainUrl+"query_pemilik.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                queryPemilik("insert")
            }
            R.id.btnUpdate ->{
                queryPemilik("update")
            }
            R.id.btnDelete ->{
                queryPemilik("delete")
            }
            R.id.btnFind -> {
                showDataPemilik(edActPemilikNama.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pemilik)
        pemilikAdapter = AdapterPemilik(daftarPemilik,this)
        lsPemilik.layoutManager = LinearLayoutManager(this)
        lsPemilik.adapter = pemilikAdapter
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    fun showDataPemilik(nmPemilik : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPemilik.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pml = HashMap<String,String>()
                    pml.put("id_pemilik",jsonObject.getString("id_pemilik"))
                    pml.put("nama_pemilik",jsonObject.getString("nama_pemilik"))
                    pml.put("nohp",jsonObject.getString("nohp"))
                    daftarPemilik.add(pml)
                }
                pemilikAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val pml = HashMap<String, String>()
                pml.put("nama_pemilik",nmPemilik)
                return pml
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryPemilik(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataPemilik("")
                    clearInputPemilik()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val pml = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        pml.put("mode","update")
                        pml.put("id_pemilik",idPemilik)
                        pml.put("nama_pemilik",edActPemilikNama.text.toString())
                        pml.put("nohp",edActPemilikNohp.text.toString())
                    }
                    "insert" -> {
                        pml.put("mode","insert")
                        pml.put("nama_pemilik",edActPemilikNama.text.toString())
                        pml.put("nohp",edActPemilikNohp.text.toString())
                    }
                    "delete" -> {
                        pml.put("mode","delete")
                        pml.put("id_pemilik",idPemilik)
                    }
                }
                return pml
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataPemilik("")
    }

    fun clearInputPemilik(){
        idPemilik = ""
        edActPemilikNama.setText("")
        edActPemilikNohp.setText("")
    }
}