package furqan.albarado.uaspendataancatering

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import furqan.albarado.ujiuas.MediaHelperMenu
import kotlinx.android.synthetic.main.activity_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MenuActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelperMenu : MediaHelperMenu
    lateinit var menuAdapter : AdapterMenu
    var daftarMenu = mutableListOf<HashMap<String,String>>()
    val mainUrl = "http://192.168.43.170/www/cat/"
    val url = mainUrl+"show_menu.php"
    var url2 = mainUrl+"query_menu.php"
    var imStr = ""
    var idMenu = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        menuAdapter = AdapterMenu(daftarMenu,this)
        mediaHelperMenu = MediaHelperMenu(this)
        lsMenu.layoutManager = LinearLayoutManager(this)
        lsMenu.adapter = menuAdapter

        imvMenu.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imvMenu -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelperMenu.getRcGallery())
            }
            R.id.btnInsert -> {
                queryMenu("insert")
            }
            R.id.btnDelete -> {
                queryMenu("delete")
            }
            R.id.btnUpdate -> {
                queryMenu("update")
            }
            R.id.btnFind -> {
                showDataMenu(edActMenuNama.text.toString().trim())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelperMenu.getRcGallery()){
                imStr = mediaHelperMenu.getBitmapToString(data!!.data,imvMenu)
            }
        }
    }

    fun queryMenu(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataMenu("")
                    clearInputMenu()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val mn = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        mn.put("mode","insert")
                        mn.put("nama_menu",edActMenuNama.text.toString())
                        mn.put("image",imStr)
                        mn.put("file",nmFile)
                    }
                    "update" -> {
                        mn.put("mode","update")
                        mn.put("id_menu",idMenu)
                        mn.put("nama_menu",edActMenuNama.text.toString())
                        mn.put("image",imStr)
                        mn.put("file",nmFile)
                    }
                    "delete" -> {
                        mn.put("mode","delete")
                        mn.put("id_menu",idMenu)
                    }
                }
                return mn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMenu(nmMenu : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var menu = HashMap<String,String>()
                    menu.put("id_menu",jsonObject.getString("id_menu"))
                    menu.put("nama_menu",jsonObject.getString("nama_menu"))
                    menu.put("url",jsonObject.getString("url"))
                    daftarMenu.add(menu)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val mn = HashMap<String, String>()
                mn.put("nama_menu",nmMenu)
                return mn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataMenu("")
    }

    fun clearInputMenu(){
        idMenu
        edActMenuNama.setText("")
    }
}