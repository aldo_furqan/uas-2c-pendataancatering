package furqan.albarado.uaspendataancatering

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_pemilik.*

class AdapterPemilik(val dataPemilik : List<HashMap<String,String>>, val pemilikActivity: PemilikActivity) :
    RecyclerView.Adapter<AdapterPemilik.HolderDataPemilik>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterPemilik.HolderDataPemilik {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_pemilik,p0,false)
        return  HolderDataPemilik(v)
    }

    override fun getItemCount(): Int {
        return dataPemilik.size
    }

    override fun onBindViewHolder(holder: AdapterPemilik.HolderDataPemilik, position: Int) {
        val data = dataPemilik.get(position)
        holder.txrowPemilikNama.setText(data.get("nama_pemilik"))
        holder.txrowPemilikNohp.setText(data.get("nohp"))
        if(position.rem(2) == 0) holder.cPemilik.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cPemilik.setBackgroundColor(Color.rgb(255,255,245))

        holder.cPemilik.setOnClickListener({
            pemilikActivity.idPemilik = data.get("id_pemilik").toString()
            pemilikActivity.edActPemilikNama.setText(data.get("nama_pemilik"))
            pemilikActivity.edActPemilikNohp.setText(data.get("nohp"))
        })
    }

    class HolderDataPemilik(v: View) : RecyclerView.ViewHolder(v) {
        val txrowPemilikNama = v.findViewById<TextView>(R.id.txrowPemilikNama)
        val txrowPemilikNohp = v.findViewById<TextView>(R.id.txrowPemilikNohp)
        val cPemilik = v.findViewById<ConstraintLayout>(R.id.cPemilik)
    }
}