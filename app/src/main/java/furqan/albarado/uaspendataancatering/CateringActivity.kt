package furqan.albarado.uaspendataancatering

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import furqan.albarado.ujiuas.MediaHelperCatering
import kotlinx.android.synthetic.main.activity_catering.*
import kotlinx.android.synthetic.main.activity_catering.btnDelete
import kotlinx.android.synthetic.main.activity_catering.btnFind
import kotlinx.android.synthetic.main.activity_catering.btnInsert
import kotlinx.android.synthetic.main.activity_catering.btnUpdate
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_pemilik.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class CateringActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelperCat : MediaHelperCatering
    lateinit var catAdapter : AdapterCatering
    lateinit var menuAdapter : ArrayAdapter<String>
    lateinit var pemilikAdapter : ArrayAdapter<String>
    var daftarCat = mutableListOf<HashMap<String,String>>()
    var daftarMenu = mutableListOf<String>()
    var daftarPemilik = mutableListOf<String>()
    val mainUrl = "http://192.168.43.170/www/cat/"
    val url = mainUrl+"show_catering.php"
    var url2 = mainUrl+"show_menu.php"
    var url3 = mainUrl+"show_pemilik.php"
    var url4 = mainUrl+"query_catering.php"
    var imStr = ""
    var pilihMenu = ""
    var pilihPemilik = ""
    var idCat = ""
    var namafile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catering)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        catAdapter = AdapterCatering(daftarCat,this)
        mediaHelperCat = MediaHelperCatering(this)
        lsCatering.layoutManager = LinearLayoutManager(this)
        lsCatering.adapter = catAdapter

        menuAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarMenu)
        spinActCateringMenu.adapter = menuAdapter
        spinActCateringMenu.onItemSelectedListener = itemSelectedMenu

        pemilikAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarPemilik)
        spinActCateringPemilik.adapter = pemilikAdapter
        spinActCateringPemilik.onItemSelectedListener = itemSelectedPemilik

        imvCatering.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imvCatering -> {
                requestPermission()
            }
            R.id.btnInsert -> {
                queryCatering("insert")
            }
            R.id.btnDelete -> {
                queryCatering("delete")
            }
            R.id.btnUpdate -> {
                queryCatering("update")
            }
            R.id.btnFind -> {
                showDataCat(edActCateringNama.text.toString().trim())
            }
        }
    }

    val itemSelectedMenu = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinActCateringMenu.setSelection(0)
            pilihMenu = daftarMenu.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMenu = daftarMenu.get(position)
        }
    }

    val itemSelectedPemilik = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinActCateringPemilik.setSelection(0)
            pilihPemilik = daftarPemilik.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihPemilik = daftarPemilik.get(position)
        }
    }

    fun requestPermission() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelperCat.getOutMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelperCat.getRcCamera())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK)
            if(requestCode == mediaHelperCat.getRcCamera()){
                imStr = mediaHelperCat.getBitmapToString(imvCatering,fileUri)
                namafile = mediaHelperCat.getMyFileName()
            }
    }

    fun queryCatering(mode : String){
        val request = object : StringRequest(
            Method.POST,url4,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataCat("")
                    clearInputCat()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val cat = HashMap<String,String>()
//                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
//                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        cat.put("mode","insert")
                        cat.put("kd_cat",edActCateringKode.text.toString())
                        cat.put("nama_cat",edActCateringNama.text.toString())
                        cat.put("email",edActCateringEmail.text.toString())
                        cat.put("image",imStr)
                        cat.put("file",namafile)
                        cat.put("nama_menu",pilihMenu)
                        cat.put("nama_pemilik",pilihPemilik)
                    }
                    "update" -> {
                        cat.put("mode","update")
                        cat.put("kd_cat",edActCateringKode.text.toString())
                        cat.put("nama_cat",edActCateringNama.text.toString())
                        cat.put("email",edActCateringEmail.text.toString())
                        cat.put("image",imStr)
                        cat.put("file",namafile)
                        cat.put("nama_menu",pilihMenu)
                        cat.put("nama_pemilik",pilihPemilik)
                    }
                    "delete" -> {
                        cat.put("mode","delete")
                        cat.put("kd_cat",edActCateringKode.text.toString())
                    }
                }
                return cat
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getMenuAndalan(mnAndalan : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMenu.add(jsonObject.getString("nama_menu"))
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val mn = HashMap<String, String>()
                mn.put("nama_menu",mnAndalan)
                return mn
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getPemilik(nmPemilik : String){
        val request = object :StringRequest(
            Request.Method.POST,url3,
            Response.Listener { response ->
                daftarPemilik.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarPemilik.add(jsonObject.getString("nama_pemilik"))
                }
                pemilikAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val pm = HashMap<String, String>()
                pm.put("nama_pemilik",nmPemilik)
                return pm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataCat(nmCat : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarCat.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var cat = HashMap<String,String>()
                    cat.put("kd_cat",jsonObject.getString("kd_cat"))
                    cat.put("nama_cat",jsonObject.getString("nama_cat"))
                    cat.put("nama_menu",jsonObject.getString("nama_menu"))
                    cat.put("email",jsonObject.getString("email"))
                    cat.put("nama_pemilik",jsonObject.getString("nama_pemilik"))
                    cat.put("url",jsonObject.getString("url"))
                    daftarCat.add(cat)
                }
                catAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val cat = HashMap<String, String>()
                cat.put("nama_cat",nmCat)
                return cat
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataCat("")
        getMenuAndalan("")
        getPemilik("")
    }

    fun clearInputCat(){
        idCat = ""
        edActCateringKode.setText("")
        edActCateringNama.setText("")
        edActCateringEmail.setText("")
    }
}