package furqan.albarado.uaspendataancatering

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val bgHeader = "header"
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgDaftar = "daftar"
    val bgMenu = "menu"
    val bgCat = "cat"
    val bgPemilik = "pemilik"
    val DEF_BACK_FITUR = ""
    val daftarVideo = intArrayOf(R.raw.vid_catering)

    var posVidSkrg = 0
    lateinit var mediaController: android.widget.MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mediaController = android.widget.MediaController(this)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val bgBack = preferences.getString(bgMain,DEF_BACK).toString()
        getColorBackground(bgBack,true)
        val bgHd = preferences.getString(bgHeader,DEF_BACK).toString()
        getColorBackground(bgHd,false)
        val bgDaftar = preferences.getString(bgDaftar,DEF_BACK_FITUR).toString()
        getColorDaftar(bgDaftar,true)
        val bgMenu = preferences.getString(bgMenu,DEF_BACK_FITUR).toString()
        getColorMenu(bgMenu,true)
        val bgCat = preferences.getString(bgCat,DEF_BACK_FITUR).toString()
        getColorCat(bgCat,true)
        val bgPemilik = preferences.getString(bgPemilik,DEF_BACK_FITUR).toString()
        getColorPemilik(bgPemilik,true)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVidSkrg)
        Toast.makeText(this,"Perubahan telah disimpan.", Toast.LENGTH_SHORT).show()

        lnDaftar.setOnClickListener(this)
        lnMenu.setOnClickListener(this)
        lnBuat.setOnClickListener(this)
        lnPemilik.setOnClickListener(this)
    }

    var nextVid = View.OnClickListener { v:View ->
        if (posVidSkrg<(daftarVideo.size-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet(posVidSkrg)
    }

    var prevVid = View.OnClickListener { v:View ->
        if (posVidSkrg>0) posVidSkrg--
        else posVidSkrg = daftarVideo.size-1
        videoSet(posVidSkrg)
    }

    fun videoSet(pos : Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun getColorBackground(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b) {
                cMain.setBackgroundColor(Color.GREEN)
                txActMainCopyright.setTextColor(Color.WHITE)
            }else{
                lnHeader.setBackgroundColor(Color.GREEN)
                txActMainHalaman.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                cMain.setBackgroundColor(Color.BLUE)
                txActMainCopyright.setTextColor(Color.WHITE)
            }else{
                lnHeader.setBackgroundColor(Color.BLUE)
                txActMainHalaman.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                cMain.setBackgroundColor(Color.YELLOW)
                txActMainCopyright.setTextColor(Color.WHITE)
            }else{
                lnHeader.setBackgroundColor(Color.YELLOW)
                txActMainHalaman.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Black")){
            if (b){
                cMain.setBackgroundColor(Color.BLACK)
                txActMainCopyright.setTextColor(Color.WHITE)
            }else{
                lnHeader.setBackgroundColor(Color.BLACK)
                txActMainHalaman.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                cMain.setBackgroundColor(Color.RED)
                txActMainCopyright.setTextColor(Color.WHITE)
            }else{
                lnHeader.setBackgroundColor(Color.RED)
                txActMainHalaman.setTextColor(Color.WHITE)
            }
        }
    }

    fun getColorDaftar(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b) {
                lnDaftar.setBackgroundColor(Color.GREEN)
                txActMainDaftar.setTextColor(Color.WHITE)
            }else{
                txActMainDaftar.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                lnDaftar.setBackgroundColor(Color.BLUE)
                txActMainDaftar.setTextColor(Color.WHITE)
            }else{
                txActMainDaftar.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                lnDaftar.setBackgroundColor(Color.YELLOW)
                txActMainDaftar.setTextColor(Color.WHITE)
            }else{
                txActMainDaftar.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Black")){
            if (b){
                lnDaftar.setBackgroundColor(Color.BLACK)
                txActMainDaftar.setTextColor(Color.WHITE)
            }else{
                txActMainDaftar.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                lnDaftar.setBackgroundColor(Color.RED)
                txActMainDaftar.setTextColor(Color.WHITE)
            }else{
                txActMainDaftar.setTextColor(Color.WHITE)
            }
        }
    }

    fun getColorMenu(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b) {
                lnMenu.setBackgroundColor(Color.GREEN)
                txActMainMenu.setTextColor(Color.WHITE)
            }else{
                txActMainMenu.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                lnMenu.setBackgroundColor(Color.BLUE)
                txActMainMenu.setTextColor(Color.WHITE)
            }else{
                txActMainMenu.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                lnMenu.setBackgroundColor(Color.YELLOW)
                txActMainMenu.setTextColor(Color.WHITE)
            }else{
                txActMainMenu.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Black")){
            if (b){
                lnMenu.setBackgroundColor(Color.BLACK)
                txActMainMenu.setTextColor(Color.WHITE)
            }else{
                txActMainMenu.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                lnMenu.setBackgroundColor(Color.RED)
                txActMainMenu.setTextColor(Color.WHITE)
            }else{
                txActMainMenu.setTextColor(Color.WHITE)
            }
        }
    }

    fun getColorCat(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b) {
                lnBuat.setBackgroundColor(Color.GREEN)
                txActMainCatering.setTextColor(Color.WHITE)
            }else{
                txActMainCatering.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                lnBuat.setBackgroundColor(Color.BLUE)
                txActMainCatering.setTextColor(Color.WHITE)
            }else{
                txActMainCatering.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                lnBuat.setBackgroundColor(Color.YELLOW)
                txActMainCatering.setTextColor(Color.WHITE)
            }else{
                txActMainCatering.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Black")){
            if (b){
                lnBuat.setBackgroundColor(Color.BLACK)
                txActMainCatering.setTextColor(Color.WHITE)
            }else{
                txActMainCatering.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                lnBuat.setBackgroundColor(Color.RED)
                txActMainCatering.setTextColor(Color.WHITE)
            }else{
                txActMainCatering.setTextColor(Color.WHITE)
            }
        }
    }

    fun getColorPemilik(myString: String, b:Boolean){
        if(myString.equals("Green")){
            if (b) {
                lnPemilik.setBackgroundColor(Color.GREEN)
                txActMainPemilik.setTextColor(Color.WHITE)
            }else{
                txActMainPemilik.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Blue")){
            if (b){
                lnPemilik.setBackgroundColor(Color.BLUE)
                txActMainPemilik.setTextColor(Color.WHITE)
            }else{
                txActMainPemilik.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (b){
                lnPemilik.setBackgroundColor(Color.YELLOW)
                txActMainPemilik.setTextColor(Color.WHITE)
            }else{
                txActMainPemilik.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Black")){
            if (b){
                lnPemilik.setBackgroundColor(Color.BLACK)
                txActMainPemilik.setTextColor(Color.WHITE)
            }else{
                txActMainPemilik.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Red")){
            if (b){
                lnPemilik.setBackgroundColor(Color.RED)
                txActMainPemilik.setTextColor(Color.WHITE)
            }else{
                txActMainPemilik.setTextColor(Color.WHITE)
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.lnDaftar->{
                val show =  Intent(this,DaftarActivity::class.java)
                startActivity(show)
            }
            R.id.lnMenu->{
                val show = Intent(this,MenuActivity::class.java)
                startActivity(show)
            }
            R.id.lnBuat->{
                val show = Intent(this,CateringActivity::class.java)
                startActivity(show)
            }
            R.id.lnPemilik->{
                val show = Intent(this,PemilikActivity::class.java)
                startActivity(show)
            }
        }
    }

}
