package furqan.albarado.uaspendataancatering

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_catering.*

class AdapterCatering(val dataCat : List<HashMap<String,String>>, val catActivity: CateringActivity) :
    RecyclerView.Adapter<AdapterCatering.HolderDataCat>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterCatering.HolderDataCat {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_catering,p0,false)
        return  HolderDataCat(v)
    }

    override fun getItemCount(): Int {
        return dataCat.size
    }

    override fun onBindViewHolder(p0: AdapterCatering.HolderDataCat, p1: Int) {
        val data = dataCat.get(p1)
        p0.txrowCatKode.setText(data.get("kd_cat"))
        p0.txrowCatNama.setText(data.get("nama_cat"))
        p0.txrowCatMenu.setText(data.get("nama_menu"))
        p0.txrowCatEmail.setText(data.get("email"))
        p0.txrowCatPemilik.setText(data.get("nama_pemilik"))
        if(p1.rem(2) == 0) p0.cCatering.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cCatering.setBackgroundColor(Color.rgb(255,255,245))

        p0.cCatering.setOnClickListener({
            val pos1 = catActivity.daftarMenu.indexOf(data.get("nama_menu"))
            val pos2 = catActivity.daftarPemilik.indexOf(data.get("nama_pemilik"))
            catActivity.spinActCateringMenu.setSelection(pos1)
            catActivity.spinActCateringPemilik.setSelection(pos2)
            catActivity.edActCateringKode.setText(data.get("kd_cat"))
            catActivity.edActCateringNama.setText(data.get("nama_cat"))
            catActivity.edActCateringEmail.setText(data.get("email"))
            Picasso.get().load(data.get("url")).into(catActivity.imvCatering)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photob)
    }

    class HolderDataCat(v: View) : RecyclerView.ViewHolder(v){
        val txrowCatKode = v.findViewById<TextView>(R.id.txrowCatKode)
        val txrowCatNama = v.findViewById<TextView>(R.id.txrowCatNama)
        val txrowCatMenu = v.findViewById<TextView>(R.id.txrowCatMenu)
        val txrowCatEmail = v.findViewById<TextView>(R.id.txrowCatEmail)
        val txrowCatPemilik = v.findViewById<TextView>(R.id.txrowCatPemilik)
        val photob = v.findViewById<ImageView>(R.id.imvRowCatering)
        val cCatering = v.findViewById<ConstraintLayout>(R.id.cCatering)
    }
}