package furqan.albarado.uaspendataancatering

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SplashActivity : AppCompatActivity(){

    lateinit var topAnim: Animation
    lateinit var bottomAnim: Animation
    lateinit var logo: ImageView
    lateinit var title: ImageView
    private lateinit var H: Handler
    private lateinit var r: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation)
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation)

        logo = findViewById(R.id.imvActSplashLogo)
        title = findViewById(R.id.imvActSplashTitle)

        logo.setAnimation(topAnim)
        title.setAnimation(bottomAnim)

        H = Handler()
        r = Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        H.postDelayed(r,4000)
    }
}