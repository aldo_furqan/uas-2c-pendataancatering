package furqan.albarado.uaspendataancatering

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_daftar.*

class AdapterDaftar(val dataDaftar : List<HashMap<String,String>>, val daftarActivity: DaftarActivity) :
    RecyclerView.Adapter<AdapterDaftar.HolderDataDaftar>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDaftar.HolderDataDaftar {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_daftar,p0,false)
        return  HolderDataDaftar(v)
    }

    override fun getItemCount(): Int {
        return dataDaftar.size
    }

    override fun onBindViewHolder(p0: AdapterDaftar.HolderDataDaftar, p1: Int) {
        val data = dataDaftar.get(p1)
        p0.txrowDafNama.setText(data.get("nama_cat"))
        p0.txrowDafMenu.setText(data.get("nama_menu"))
        p0.txrowDafEmail.setText(data.get("email"))
        p0.txrowDafPemilik.setText(data.get("nama_pemilik"))
        if(p1.rem(2) == 0) p0.cDaftar.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cDaftar.setBackgroundColor(Color.rgb(255,255,245))

        p0.cDaftar.setOnClickListener({
            daftarActivity.edActDaftarNama.setText(data.get("nama_cat"))
        })

        if(!data.get("urlmenu").equals(""))
            Picasso.get().load(data.get("urlmenu")).into(p0.photoa)

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photob)
    }

    class HolderDataDaftar(v: View) : RecyclerView.ViewHolder(v){
        val txrowDafNama = v.findViewById<TextView>(R.id.txrowDafNama)
        val txrowDafMenu = v.findViewById<TextView>(R.id.txrowDafMenu)
        val txrowDafEmail = v.findViewById<TextView>(R.id.txrowDafEmail)
        val txrowDafPemilik = v.findViewById<TextView>(R.id.txrowDafPemilik)
        val photoa = v.findViewById<ImageView>(R.id.imvrowDafMenu)
        val photob = v.findViewById<ImageView>(R.id.imvrowDafCat)
        val cDaftar = v.findViewById<ConstraintLayout>(R.id.cDaftar)
    }
}